# Howto: jekyll + docker

``` shell
# Get initial file
$ wget https://gitlab.com/pewen/jekyll-docker/-/archive/master/jekyll-docker-master.zip
$ unzip https://gitlab.com/pewen/jekyll-docker/-/archive/master/jekyll-docker-master.zip
$ cd jekyll-docker-master

# opcionalmente se pueden actualizar las gemas
$ docker-compose run web bundle update --all

# init jekyll
$ docker-compose run --rm web jekyll new . --force

# Fun with jekyll
$ docker-compose up

```
